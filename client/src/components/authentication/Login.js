import React, { useState, useContext, useEffect } from 'react'
import AuthContext from '../../context/auth/authContext';
import AlertContext from '../../context/alert/AlertContext';
import styles from './Login.module.css'
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


const Login = (props) => {
    const alertContext = useContext(AlertContext);
    const authContext = useContext(AuthContext);
    const { setAlert } = alertContext;

    const { login, error, clearErrors, isAuthenticated } = authContext

    useEffect(() => {
        if (isAuthenticated) {
            props.history.push('/')
        }


        if (error === 'Invalid Credentials') {
            setAlert(error, 'danger');
            clearErrors();
        }
        //eslint-disabled-next-line
    }, [error, isAuthenticated, props.history]);

    const [user, setUser] = useState({
        email: '',
        password: '',
    });

    const notifySuccess = (msg) => {
        toast.success(msg, {
            position: toast.POSITION.TOP_RIGHT
        });
    }
    const notifyError = (msg) => {
        toast.error(msg, {
            position: toast.POSITION.TOP_RIGHT
        });
    }

    const { email, password } = user;

    const onChange = e => setUser({ ...user, [e.target.name]: e.target.value });

    const onSubmit = e => {
        e.preventDefault();
        if (email === '' || password === '') {
            notifyError("please enter all fields");
            // setAlert('Please fill in all fields', 'danger')
        } else {
            login({
                email,
                password
            })
        }
    }

    return (

        <div className='form-container' className={styles.box}>
            <ToastContainer />

            <h2>
                Account Login
            </h2>
            <form onSubmit={onSubmit}>
                <div className="className={styles.inpuBox}" className={styles.inpuBox}>
                    <label htmlFor="email">Email Address</label>
                    <input type="text" name="email" value={email} onChange={onChange} placeholder="Your Email" required />
                </div>
                <div className="form-group" className={styles.inpuBox}>
                    <label htmlFor="password">Password</label>
                    <input type="password" name="password" value={password} onChange={onChange} placeholder="Your Password" required />
                </div>

                <input type="submit" value='Login' className="btn btn-primary btn-block" />
            </form>
        </div>
    )
}
export default Login